extern crate zip;
#[macro_use] extern crate lazy_static;

use std::path::{Path, PathBuf};
use std::process::Command;
use std::sync::{Arc, Mutex};
use std::ops::Deref;

mod unzip;

pub use unzip::unzip_archive;

lazy_static! {
    static ref CURRENT_UID: Arc<Mutex<usize>> = Arc::new(Mutex::new(0));
}

pub struct Assignment {
    container_name: String,
    test_file: PathBuf,
    tmp_dir: PathBuf
}

impl Assignment {
    pub fn new(container_name: String, test_file: PathBuf, tmp_dir: PathBuf) -> Assignment {
        Assignment {
            container_name,
            test_file,
            tmp_dir
        }
    }

    pub fn grade(&self, source_zip: PathBuf) -> Result<Score, String> {
        let test_file_path = self.test_file.canonicalize()
            .or::<String>(Err("Bad test file path".into()))?;

        let uid = get_uid();
        let tmp_dir = self.tmp_dir.join(Path::new(&uid.to_string()));
        // Creates a temp dir which is automatically removed when this goes out of scope
        let unzip_dir = TempDir::new(tmp_dir)?;

        if let Err(e) = unzip::unzip_archive(&source_zip, &unzip_dir) {
            return Err(format!("Failed to unzip source file: {}", e));
        }

        // Copy the test file into the temp directory
        if let Err(e) = std::fs::copy(
            &test_file_path,
            &unzip_dir.join(&test_file_path.file_name().unwrap())
        ) {
            return Err(format!("Failed to copy test file: {}", e));
        };

        // Run the docker job
        let output = match Command::new("docker")
                                   .args(&["run", "-t", "-v",
                                         &format!("{}:/workdir/source:ro", unzip_dir.to_string_lossy()),
                                         &self.container_name])
                                   .output()  {
            Ok(o) => o,
            Err(e) => return Err(format!("Failed to run docker job: {}", e))
        };

        Ok(Score {
            tests: 0,
            passed: 0,
            output: String::from_utf8_lossy(&output.stdout).into_owned()
        })
    }
}


pub struct Score {
    pub tests: usize,
    pub passed: usize,
    pub output: String
}

/**
A wrapper around a PathBuf which is created when initialized and removed when dropped.
*/
struct TempDir(PathBuf);
impl TempDir {
    fn new(path: PathBuf) -> Result<TempDir, String> {
        std::fs::create_dir_all(&path)
            .or::<String>(Err("Failed to create temp dir".into()))?;

        Ok(TempDir(path))
    }
}
impl Drop for TempDir {
    fn drop(&mut self) {
        // If the remove fails, there's nothing we can do so we just ignore it
        std::fs::remove_dir_all(self.0.to_string_lossy().into_owned()).ok();
    }
}
impl Deref for TempDir {
    type Target = PathBuf;
    fn deref(&self) -> &PathBuf { &self.0 }
}


fn get_uid() -> usize {
    let mut cur_id = CURRENT_UID.lock().unwrap();
    let uid = *cur_id;
    *cur_id += 1;

    uid
}
