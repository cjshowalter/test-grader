extern crate test_grader;

use std::path::Path;

fn main() {
    let assignment = test_grader::Assignment::new(
        "unittest:cpp".into(),
        Path::new("../docker-test-runners/cpp/example_code/example_test.cpp").to_path_buf(),
        Path::new("/tmp/assignments").to_path_buf()
    );

    println!("{}", assignment.grade(
            Path::new("test.zip").to_path_buf()
        ).unwrap().output
    );
}
