use std::fs::File;
use std::path::Path;
use std::io::{Write, Read};


static BUF_SIZE: usize = 4096;


/**
Reads a zip file from `src` and writes the decompressed contents into the directory `dst`.
# Examples

```rust,no_run
use std::path::Path;
use test_grader::unzip_archive;

// test/ will contain the contents of test.zip
unzip_archive(Path::new("test.zip"), Path::new("test/")).unwrap();
```

# Errors

Any IO errors will be propagated out.
*/
pub fn unzip_archive(src: &Path, dst: &Path) -> std::io::Result<()> {
    let file = File::open(src)?;
    let mut zip = zip::ZipArchive::new(file)?;

    for i in 0..zip.len() {
        let file = zip.by_index(i).unwrap();
        unzip_file(file, dst)?;
    }

    Ok(())
}

fn unzip_file(mut file: zip::read::ZipFile, dst: &Path) -> std::io::Result<()> {
    let path = file.sanitized_name();
    let final_path = dst.join(path);

    // Create the parent directory if needed
    if let Some(path) = final_path.parent() {
        std::fs::create_dir_all(path)?;
    }

    let mut outfile = File::create(final_path)?;
    copy_file_buffered(&mut file, &mut outfile)?;

    Ok(())
}

fn copy_file_buffered(infile: &mut zip::read::ZipFile, outfile: &mut Write) -> std::io::Result<()> {
    let file_size = infile.size() as usize;
    let buf_size = if file_size < BUF_SIZE { file_size } else { BUF_SIZE };
    let mut buf = vec![0; buf_size];

    loop {
        let n = infile.read(buf.as_mut_slice())?;
        if n == 0 { break; }

        outfile.write(&buf[..n])?;
    }

    Ok(())
}
